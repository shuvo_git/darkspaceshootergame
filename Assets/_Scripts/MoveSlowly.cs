﻿using UnityEngine;
using System.Collections;

public class MoveSlowly : MonoBehaviour {

	public Vector3 v3StartPos;
	public Vector3 v3EndPos;
	public float fDistPerSecond = 0.1f;


	
	void Start () 
	{
		transform.position = v3StartPos;
	}
	
	void Update () 
	{
		transform.position = Vector3.MoveTowards(transform.position, v3EndPos, fDistPerSecond * Time.deltaTime);
		//Debug.Log (transform.position.z - v3StartPos.z);
		if ((v3EndPos.z - transform.position.z) < 4.75f) {
			transform.position = v3StartPos;
			//transform.rotation = new Quaternion(180);
		}
			//Debug.Log ("reach back end");


	}
}
