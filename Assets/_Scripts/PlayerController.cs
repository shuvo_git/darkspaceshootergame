﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary{
	public float xMin;
	public float xMax;
}

public class PlayerController : MonoBehaviour {

	private RuntimePlatform platform;

	public float speed;
	public Boundary boundary;
	public float tilt;

	public GameObject shot;
	public Transform shotSpawn;

	public float fireRate;
	private float nextFire;

	void Update ()
	{
		if ( (Input.GetKeyDown(KeyCode.F) || Input.touchCount>0 ) && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);

			GetComponent<AudioSource>().Play();
		}
	}

	void FixedUpdate()
	{
		float moveHorizontal;
		platform = Application.platform;

		if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer)
			moveHorizontal = Input.acceleration.x;
		else
			moveHorizontal = Input.GetAxis ("Horizontal");


		Vector3 movement = new Vector3(moveHorizontal,0.0f,0.0f);
		GetComponent<Rigidbody>().velocity = movement * speed;

		GetComponent<Rigidbody>().position = new Vector3 (Mathf.Clamp(GetComponent<Rigidbody>().position.x,boundary.xMin,boundary.xMax),0.0f,0.0f); 
	
		GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f,0.0f,GetComponent<Rigidbody>().velocity.x*-tilt);  
	}
}

//float moveVertical = 0.0f;//Input.GetAxis("Mouse Y");

/*if (Input.touchCount > 0)
		{
			moveHorizontal = Input.touches[0].deltaPosition.x;
			moveVertical   = Input.touches[0].deltaPosition.y;
		}*/
