﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour {

	public GameObject hazard;
	public Vector3 spawnValues;

	public int hazardCount;
	public float startWait;
	public float spawnWait;

	public GUIText scoreText;
	public GUIText gameOverText;
	public Button restartButton;

	private bool gameOver;
	private bool restart;

	private int score;






	void Start () 
	{
		gameOver = false;
		restart = false;
		gameOverText.enabled = false;
		//restartButton = (Button) GameObject.FindGameObjectsWithTag ("RestartButton");
		restartButton.gameObject.SetActive (false);

		score = 0;
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
	}

	void Update()
	{
		if (restart) 
		{
			restart = false;
			restartButton.onClick.AddListener(()=>
			{

				//if(Input.touchCount>0 || Input.GetKey(KeyCode.R))
				Application.LoadLevel(Application.loadedLevel);
				//Debug.Log("restart loaded");
					
			});

		}
	}

	IEnumerator SpawnWaves()
	{
		while(true)
		{
			yield return new WaitForSeconds(startWait);

			for (int i=0; i<hazardCount; i++) 
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);

				yield return new WaitForSeconds(spawnWait);
			}
			if(gameOver)
			{
				restartButton.gameObject.SetActive(true);
				restart = true;
				break;
			}
		}
	}

	public void AddScore(int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}


	void UpdateScore()
	{
		scoreText.text = "Score :  " + score;
	}

	public void GameOver()
	{
		gameOverText.enabled = true;
		gameOver = true;

	}


}













